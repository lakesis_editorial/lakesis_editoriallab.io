+++
title =  "Herbert Marcuse - Tolerancia Represiva"
categories = "política"
tags = ["zine", "marxismo", "política"]
date = "2022-11-12"
description = "Una crítica al carácter represivo de toda tolerancia abstracta"
images = ["https://gitlab.com/lakesis_editorial/lakesis_editorial.gitlab.io/-/raw/main/media/tolerancia-represiva-banner.jpg"]
cover = "https://gitlab.com/lakesis_editorial/lakesis_editorial.gitlab.io/-/raw/main/media/tolerancia-represiva-banner.jpg"
+++

![Imagen](https://gitlab.com/lakesis_editorial/lakesis_editorial.gitlab.io/-/raw/main/media/tolerancia-represiva-banner.jpg)

En este ensayo (1965), Herbert Marcuse critica la utilización de una tolerancia abstracta como criterio para determinar qué tipo de prácticas o políticas son aceptables para una sociedad libre. Este tipo de tolerancia, en la práctica, tiene un efecto represivo sobre las luchas subversivas: tolera el orden de clases imperante pero a la vez reprime todo intento de superar la violencia que las clases dominantes defienden.

**Descarga el zine en sus dos versiones**:

[**Digital**](https://gitlab.com/lakesis_editorial/textos/-/raw/main/Herbert-Marcuse-Tolerancia-Represiva/Herbert%20Marcuse%20-%20%20Tolerancia%20Represiva%20-%20digital.pdf)

[**Zine**](https://gitlab.com/lakesis_editorial/textos/-/raw/main/Herbert-Marcuse-Tolerancia-Represiva/Herbert%20Marcuse%20-%20%20Tolerancia%20Represiva%20-%20zine.pdf)

