+++
title =  "Carlos Pérez Soto - Violencia del Derecho y Derecho a la Violencia"
categories = "política"
tags = ["zine", "marxismo", "política"]
date = "2022-11-12"
description = "Crítica a la violencia del orden jurídico y una defensa de la violencia revolucionaria"
images = ["https://gitlab.com/lakesis_editorial/lakesis_editorial.gitlab.io/-/raw/main/media/derecho-a-la-violencia-banner.jpg"]
cover = "https://gitlab.com/lakesis_editorial/lakesis_editorial.gitlab.io/-/raw/main/media/derecho-a-la-violencia-banner.jpg"
+++

![Imagen](https://gitlab.com/lakesis_editorial/lakesis_editorial.gitlab.io/-/raw/main/media/derecho-a-la-violencia-banner.jpg)

El orden jurídico, creado por la burguesía, contiene los mecanismos de legitimación para su propia violencia: la explotación. Ante esa violencia institucionalizada, tenemos derecho a la violencia revolucionaria de masas para construir una sociedad sin clases: el horizonte comunista.

**Descarga el zine en sus dos versiones**:

[**Digital**](https://gitlab.com/lakesis_editorial/textos/-/raw/main/Carlos-Perez-Soto-violencia-del-derecho/Carlos%20P%C3%A9rez%20Soto%20-%20Violencia%20del%20derecho%20-%20digital.pdf)

[**Zine**](https://gitlab.com/lakesis_editorial/textos/-/raw/main/Carlos-Perez-Soto-violencia-del-derecho/Carlos%20P%C3%A9rez%20Soto%20-%20Violencia%20del%20derecho%20-%20zine.pdf)

